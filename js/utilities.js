"uses strict";

function addHTMLElement(tagname, content, parent) {
    let new_node = document.createElement(tagname);
    new_node.textContent=content;
    let par = document.getElementById(parent);
    par.append(new_node);
    return new_node;
}

function changeTextColor(tagname, textColor) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.color = textColor;
    }
}

function changeBackground(tagname, backgroundColor) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.backgroundColor = backgroundColor;
    }
}

function changeTagWidth(tagname, percentage) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.width = percentage + "%";
    }
}

function changeBorderColor(tagname, borderColor) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.borderColor = borderColor;
    }
}

function changeBorderWidth(tagname, pixel) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.borderWidth = pixel + "px";
    }
}
