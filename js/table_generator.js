"uses strict";

window.addEventListener('DOMContentLoaded', function () {
    console.log('DOM fully loaded and parsed');

    let row_count = document.getElementById("row_count");
    row_count.addEventListener("keyup", generateTable);

    let column_count = document.getElementById("column_count");
    column_count.addEventListener("keyup", generateTable);

    let table_width = document.getElementById("table_width");
    table_width.addEventListener("change", generateTable);

    let text_color = document.getElementById("text_color");
    text_color.addEventListener("change", generateTable);

    let background_color = document.getElementById("background_color");
    background_color.addEventListener("change", generateTable);

    let border_width = document.getElementById("border_width");
    border_width.addEventListener("keyup", generateTable);

    let border_color = document.getElementById("border_color");
    border_color.addEventListener("change", generateTable);
});

function generateTable() {

    if(document.getElementById("table-created") !=null) {
        document.getElementById("table-created").remove();
    }

    let new_table = addHTMLElement("table", null, "table-render-space");
    new_table.id = "table-created";

    let row_count = getRowCount();
    let column_count = getColumnCount();

    for (let i = 0; i < row_count; ++i) {

        let currentTableRow = addHTMLElement("tr", null, "table-created");
        let tableRow = "tr" + i;

        currentTableRow.id = tableRow;

        for(let j = 0; j < column_count; ++j) {
            addHTMLElement("td", "cell" + i + j, tableRow);
        }
    }

    changeTagWidth("table", getTableWidth());
    changeTextColor("table", getTextColor());
    changeBackground("td", getBackgroundColor());
    changeBorderWidth("td", getBorderWidth());
    changeBorderColor("td", getBorderColor());

    generateHTML();
}

function generateHTML() {
    if (document.querySelector("textarea") != null) {
        document.querySelector("textarea").remove();
    }
    addHTMLElement("textarea", null, "table-html-space");
    document.querySelector("textarea").readOnly=true;

    let textArea = "<table>\n";
    for (let i = 0; i < getRowCount(); ++i) {
        textArea += "    <tr>\n";
        for (let j = 0; j < getColumnCount(); ++j) {
            textArea += "      <td>cell" + i + j + "</td>\n";
        }
        textArea += "    </tr>\n";
    }
    textArea += "</table>";

    document.querySelector("textarea").value = textArea;
}

function getRowCount() {
    let row_count = document.getElementById("row_count").value;
    return row_count;
}

function getColumnCount() {
    let column_count = document.getElementById("column_count").value;
    return column_count;
}

function getTableWidth() {
    let table_width = document.getElementById("table_width").value;
    return table_width;
}

function getTextColor() {
    let text_color = document.getElementById("text_color").value;
    return text_color;
}

function getBackgroundColor() {
    let background_color = document.getElementById("background_color").value;
    return background_color;
}

function getBorderWidth() {
    let border_width = document.getElementById("border_width").value;
    return border_width;
}

function getBorderColor() {
    let border_color = document.getElementById("border_color").value;
    return border_color;
}